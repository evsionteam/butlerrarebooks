<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package butlerrarebooks
 */
global $brb_options;
?>

</div><!-- #content -->


<!-- Footer section -->
<footer id="colophon" class="site-footer" role="contentinfo">

    <div class="container">
        <div class="row">

            <div class="col-sm-7 col-md-8">
                <!-- phone-no-n-email-address -->
                <div class="phone-no-n-email-address">

                    <!-- phone-no -->
                    <div class="phone-no"><a href="tel:<?php echo $brb_options['brb_tel_no'];?>"><i class="fa fa-phone"></i><span><?php echo $brb_options['brb_tel_no'];?></span></a></div>
                    <!-- email-address -->
                    <div class="email-address"><a href="mailto:<?php echo $brb_options['brb_mail'];?>"><i class="fa fa-envelope"></i><span><?php echo $brb_options['brb_mail'];?></span></a></div>
                </div><!-- phone-no-n-email-address -->
            </div><!-- /col-md-8  -->

            <div class="col-sm-5 col-md-4">
                <!-- copyright section -->
                <div class="site-info">
                    <?php echo $brb_options['brb_copyright'];?>
                </div><!-- .site-info /copyright section -->
            </div><!-- col-md-4 -->


        </div><!-- /row -->
    </div><!-- /container -->                

</footer><!-- #colophon /Footer section -->

</div><!-- #page -->

<?php wp_footer(); ?>
</body>

</html>
