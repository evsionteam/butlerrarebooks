<?php

if (!function_exists('butlerrarebooks_setup')) :

    function butlerrarebooks_setup() {

        load_theme_textdomain('butlerrarebooks', get_template_directory() . '/languages');
        add_theme_support('automatic-feed-links');
        add_theme_support('title-tag');
        add_theme_support('post-thumbnails');
        register_nav_menus(array(
            'primary' => esc_html__('Primary', 'butlerrarebooks'),
        ));

        //custom image size
        add_image_size( 'brb-main-slider', 160, 275, true );

        add_theme_support('html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ));

        add_theme_support('post-formats', array(
            'aside',
            'image',
            'video',
            'quote',
            'link',
        ));

        add_theme_support( 'woocommerce' );
    }

endif;

add_action('after_setup_theme', 'butlerrarebooks_setup');

function butlerrarebooks_content_width() {
    $GLOBALS['content_width'] = apply_filters('butlerrarebooks_content_width', 640);
}

add_action('after_setup_theme', 'butlerrarebooks_content_width', 0);

function butlerrarebooks_widgets_init() {
    register_sidebar(array(
        'name' => esc_html__('Sidebar', 'butlerrarebooks'),
        'id' => 'sidebar-1',
        'description' => '',
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>',
    ));
}

add_action('widgets_init', 'butlerrarebooks_widgets_init');

function butlerrarebooks_scripts() {

    wp_enqueue_style('butlerrarebooks-font', get_template_directory_uri() . '/font-awesome-4.4.0/css/font-awesome.min.css');
    wp_enqueue_style( 'butlerrarebooks-gfont', '//fonts.googleapis.com/css?family=Lato:400,300,700' );
    wp_enqueue_style('butlerrarebooks-bootmin', get_template_directory_uri() . '/css/bootstrap.min.css');
    wp_enqueue_style('butlerrarebooks-main', get_template_directory_uri() . '/css/main.css');
    wp_enqueue_style('butlerrarebooks-animate', get_template_directory_uri() . '/css/animate.css');
    wp_enqueue_style('butlerrarebooks-style', get_stylesheet_uri());
    
    wp_enqueue_script('butlerrarebooks-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true);
    wp_enqueue_script('butlerrarebooks-vendor', get_template_directory_uri() . '/js/vendor/npm.js', array('jquery'), '', true);
    wp_enqueue_script('butlerrarebooks-modernizer', get_template_directory_uri() . '/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js', '', '', true);
    wp_enqueue_script('butlerrarebooks-boot', get_template_directory_uri() . '/js/vendor/bootstrap.js', '', '', true);
    wp_enqueue_script('butlerrarebooks-caroufred', get_template_directory_uri() . '/js/vendor/jquery.carouFredSel-6.0.0.js', '', '', true);
    wp_enqueue_script('butlerrarebooks-nicescroll', get_template_directory_uri() . '/js/vendor/nicescroll.js', '', '', true);
    wp_enqueue_script('butlerrarebooks-main', get_template_directory_uri() . '/js/main.js', '', '', true);

    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
}

add_action('wp_enqueue_scripts', 'butlerrarebooks_scripts');

add_action('init', 'brb_add_excerpts_to_pages');

function brb_add_excerpts_to_pages() {
    add_post_type_support('page', 'excerpt');
}

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

require get_template_directory() . '/inc/template-tags.php';

require get_template_directory() . '/inc/extras.php';

require get_template_directory() . '/inc/customizer.php';

require get_template_directory() . '/inc/wc_brb_options.php';

function search_by_book_cat($query) {
    if ($query->is_search()) {
        $term = $_GET['category'];
        if (isset($term) && !empty($term)) {
            if ('all' != $term) {
                $tax_query = array(
                    array(
                        'taxonomy' => 'product_cat',
                        'field' => 'slug',
                        'terms' => $term,
                    )
                );
                $query->set('tax_query', $tax_query);
            } else {
                $query->set('post_type', 'product');
            }
        }
        else{
            $query->set('post_type', array('product'));
        }
    }
}

add_action('pre_get_posts', 'search_by_book_cat', 11);

//brb options
$brb_options = get_theme_mod('brb_option');

//add product title to meta when saving product
add_action( 'save_post_product', 'brb_save_product_info_meta' );
function brb_save_product_info_meta() {
    $title = get_the_title();
    update_post_meta(get_the_ID(),'book_name',$title);
    $page = get_permalink(get_the_ID());
    update_post_meta(get_the_ID(),'book_page',$page);
}

//add pagination
function brb_pagination($pages = '', $range = 4)
{
    $showitems = ($range * 2)+1;
    global $paged;
    if(empty($paged)) $paged = 1;

    if($pages == '')
    {
        global $wp_query;
        $pages = $wp_query->max_num_pages;
        if(!$pages)
        {
            $pages = 1;
        }
    }

    if(1 != $pages)
    {
        echo "<div class=\"pagination\"><span>Page ".$paged." of ".$pages."</span>";
        if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>&laquo; First</a>";
        if($paged > 1 && $showitems < $pages) echo "<a href='".get_pagenum_link($paged - 1)."'>&lsaquo; Previous</a>";

        for ($i=1; $i <= $pages; $i++)
        {
            if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
            {
                echo ($paged == $i)? "<span class=\"current\">".$i."</span>":"<a href='".get_pagenum_link($i)."' class=\"inactive\">".$i."</a>";
            }
        }

        if ($paged < $pages && $showitems < $pages) echo "<a href=\"".get_pagenum_link($paged + 1)."\">Next &rsaquo;</a>";
        if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."'>Last &raquo;</a>";
        echo "</div>\n";
    }
}
