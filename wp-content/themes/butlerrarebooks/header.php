<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package butlerrarebooks
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
        <?php wp_head(); ?>
    </head>
    <body <?php body_class(); ?>>
        <div id="page" class="site">
            <a class="skip-link screen-reader-text" href="#content"><?php esc_html_e('Skip to content', 'butlerrarebooks'); ?></a>

            <!--Top Header section-->
            <header id="masthead" class="site-header" role="banner">
                <div class="container">
                    <div class="row">

                        <div class="col-xs-6 col-sm-4 col-md-3">
                            <!-- logo section  -->
                            <div class="site-branding">
                                <!-- logo section -->
                                <div class="logo-section <?php if(!is_front_page()):echo 'wt-shadow';endif;?>">
                                    <a href="<?php echo esc_url(home_url('/')); ?>" rel="home">
                                        <img src="<?php header_image();?>" class="img-responsive" alt="logo image"/>
                                    </a>
                                </div><!-- logo-section -->
                            </div><!-- .site-branding / logo-section -->
                        </div><!-- col-md-3 -->

                        <div class="col-xs-6 col-sm-8 col-md-9">
                            <!-- navigation menu section -->
                            <nav id="site-navigation" class="main-navigation clearfix" role="navigation">
                                <button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><i class="fa fa-bars"></i></button>
                                <?php wp_nav_menu(array('theme_location' => 'primary', 'menu_id' => 'primary-menu')); ?>
                            </nav><!-- #site-navigation -->
                        </div><!-- col-md-9 -->

                    </div><!-- /row -->
                </div><!-- /container -->                
            </header><!-- #masthead  /top header section -->

            <div id="content" class="site-content <?php if(!is_front_page()):echo 'common-inner-page';endif;?>">
