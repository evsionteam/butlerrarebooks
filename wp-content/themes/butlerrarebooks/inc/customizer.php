<?php
function brb_customizer( $wp_customize ) {

    $wp_customize->add_panel(
        'brb_panel',
        array(
            'title'          =>__( 'Butler Options', 'butlerrarebooks' ),
            'description'    => __( 'Customize Theme settings here', 'butlerrarebooks' ),
        )
    );

    $wp_customize->add_section(
        'brb_home_section',
        array(
            'title' => __( 'Home Page Options', 'butlerrarebooks' ),
            'description' => __( 'Customize Home Page options Here !!!', 'butlerrarebooks' ),
            'panel'  => 'brb_panel'
        )
    );

    $wp_customize->add_setting(
        'brb_option[brb_about_page]',
        array(
            'sanitize_callback' => 'brb_sanitize_integer',
        )
    );
    $wp_customize->add_control(
        'brb_option[brb_about_page]',
        array(
            'type' => 'dropdown-pages',
            'section' => 'brb_home_section',
            'label' => __( 'Choose About Us page', 'butlerrarebooks' )
    ) );

    $wp_customize->add_section(
        'brb_footer_section',
        array(
            'title' => __( 'Footer Options', 'butlerrarebooks' ),
            'description' => __( 'Customize Footer options Here !!!', 'butlerrarebooks' ),
            'panel'  => 'brb_panel'
        )
    );

    $wp_customize->add_setting(
        'brb_option[brb_tel_no]',
        array(
            'sanitize_callback' => 'brb_sanitize_text'
        )
    );
    $wp_customize->add_control(
        'brb_option[brb_tel_no]',
        array(
            'type' => 'text',
            'section' => 'brb_footer_section',
            'label' => __( 'Enter Telephone no:', 'butlerrarebooks' )
        )
    );

    $wp_customize->add_setting(
        'brb_option[brb_mail]',
        array(
            'sanitize_callback' => 'brb_sanitize_text'
        )
    );
    $wp_customize->add_control(
        'brb_option[brb_mail]',
        array(
            'type' => 'text',
            'section' => 'brb_footer_section',
            'label' => __( 'Enter Mail Address:', 'butlerrarebooks' )
        )
    );

    $wp_customize->add_setting(
        'brb_option[brb_copyright]'
    );
    $wp_customize->add_control(
        'brb_option[brb_copyright]',
        array(
            'type' => 'textarea',
            'section' => 'brb_footer_section',
            'label' => __( 'Enter Copyright Text:', 'butlerrarebooks' )
        )
    );

    $wp_customize->add_section(
        'brb_social_section',
        array(
            'title' => __( 'Social Options', 'butlerrarebooks' ),
            'description' => __( 'Customize Social options Here !!!', 'butlerrarebooks' ),
            'panel'  => 'brb_panel'
        )
    );

    $wp_customize->add_setting(
        'brb_option[brb_facebook_link]'
    );
    $wp_customize->add_control(
        'brb_option[brb_facebook_link]',
        array(
            'type' => 'text',
            'section' => 'brb_social_section',
            'label' => __( 'Enter Facebook Url:', 'butlerrarebooks' )
        )
    );

    $wp_customize->add_setting(
        'brb_option[brb_twitter_link]'
    );
    $wp_customize->add_control(
        'brb_option[brb_twitter_link]',
        array(
            'type' => 'text',
            'section' => 'brb_social_section',
            'label' => __( 'Enter Twitter Url:', 'butlerrarebooks' )
        )
    );

    $wp_customize->add_setting(
        'brb_option[brb_youtube_link]'
    );
    $wp_customize->add_control(
        'brb_option[brb_youtube_link]',
        array(
            'type' => 'text',
            'section' => 'brb_social_section',
            'label' => __( 'Enter Youtube Url:', 'butlerrarebooks' )
        )
    );

    $wp_customize->add_setting(
        'brb_option[brb_linkedin_link]'
    );
    $wp_customize->add_control(
        'brb_option[brb_linkedin_link]',
        array(
            'type' => 'text',
            'section' => 'brb_social_section',
            'label' => __( 'Enter Linkedin Url:', 'butlerrarebooks' )
        )
    );
}
add_action( 'customize_register', 'brb_customizer' );
function brb_sanitize_integer( $input ) {
    if( is_numeric( $input ) ) {
        return intval( $input );
    }
}
function brb_sanitize_text( $input ) {
    return wp_kses_post( force_balance_tags( $input ) );
}