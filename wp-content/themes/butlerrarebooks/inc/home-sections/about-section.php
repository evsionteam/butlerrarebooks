<!--ABOUT US SECTION-->
<div class="about-us-section">
    <?php
    global $brb_options;
    /* About Us Page */
    $about_page = $brb_options['brb_about_page'];
    if (!empty($about_page)):
        $about_page_details = get_post($about_page);
        ?>
        <!--ABOUT US SECTION-->
        <div class="col-sm-5 col-md-4">
            <div class="about-img">
                <?php echo get_the_post_thumbnail($about_page); ?>
            </div><!-- about-img -->
        </div><!-- col-md-4 -->

        <div class="col-sm-7 col-md-8">
            <div class="about-description">
                <div class="about-title">
                    <?php echo $about_page_details->post_title; ?>
                </div><!-- about-title -->
                <div class="about-content">
                    <?php echo $about_page_details->post_excerpt; ?>
                </div><!-- about-content -->
                <div class="brb-more">
                    <a href="<?php the_permalink($about_page) ?>"><?php _e('More', 'butlerrarebooks') ?></a>
                </div><!-- brb-more -->
            </div><!-- about-description -->
        </div><!-- col-md-8 -->
    <?php endif; ?>
</div><!-- /about-us-section  -->