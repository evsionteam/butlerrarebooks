<!--Featured Books wrapper-->
<div class="featured-books-wrapper">
    <div class="container">
        <div class="row">

            <!--feature book title-->
            <div class="col-md-12">
                <div class="featured-book-titl">
                    <h1><?php _e('Featured Book', 'butlerrarebooks'); ?></h1>
                </div><!-- /featured-book-titl -->
            </div>
            <div class="feature-slider-wrapper-section clearfix">
                <!--next and previous button-->
                <div id="pre" class="button hvr-bounce-out" alt="pre button"><img src="<?php echo get_template_directory_uri() ?>/images/left-arrow.png" alt="left arrow"/></div>
                <div id="next" class="button hvr-bounce-out" alt="pre button"><img src="<?php echo get_template_directory_uri() ?>/images/right-arrow.png" alt="left arrow"/></div>

                <!--slider section-->
                <div class="feature-slider-wrapper clearfix">
                    <?php
                    $args = array(
                        'post_type' => 'product',
                        'posts_per_page' => -1,
                        'post_status' => 'published',
                        'meta_query' => array(
                            array(
                                'key' => '_featured',
                                'value' => 'yes',
                                'compare' => '='
                            )
                        )
                    );
                    $featured_books = new WP_Query($args);
                    if ($featured_books->have_posts()) :
                        while ($featured_books->have_posts()) :$featured_books->the_post();
                            global $product;
                            ?>
                            <div class="col-xs-12 col-sm-6">
                                <div class="featured-books">

                                    <!--Description-->
                                    <div class="description">
                                        <div class="ft-book-title">
                                            <a href="<?php the_permalink() ?>"><h2><?php echo $product->get_title(); ?></h2></a>
                                        </div>
                                        <?php $price = $product->get_price();if(!empty($price)):?>
                                        <div class="ft-book-price">
                                            <h3>
                                                <?php
                                                echo get_woocommerce_currency_symbol();
                                                echo $price;
                                                ?>
                                            </h3>
                                        </div>
                                        <?php endif;?>
                                        <div class="ft-book-info">
                                            <?php
                                            //book author
                                            $book_author = get_post_meta(get_the_ID(), 'book-author', true);
                                            if (!empty($book_author)):
                                                echo '<div class="book-author">';
                                                echo '<span>' . __('Author', 'butlerrarebooks') . '</span>:' . $book_author;
                                                echo '</div>';
                                            endif;

                                            //publication date
                                            $book_published = get_post_meta(get_the_ID(), 'book-published', true);
                                            if (!empty($book_published)):
                                                echo '<div class="book-published">';
                                                echo '<span>' . __('Publication Date', 'butlerrarebooks') . '</span>:' . $book_published;
                                                echo '</div>';
                                            endif;
                                            ?>
                                        </div>
                                        <div class="short-dec">
                                            <?php echo wpautop(wp_trim_words(get_the_content(), 20)); ?>
                                        </div>

                                        <div class="brb-more">
                                            <a href="<?php the_permalink() ?>"><?php _e('More', 'butlerrarebooks') ?></a>
                                        </div>
                                    </div><!-- /description -->

                                    <!--ft-book-img-->
                                    <div class="ft-book-img">
                                        <?php
                                        if(has_post_thumbnail()):
                                            the_post_thumbnail('shop_single');
                                        else:
                                            echo wc_placeholder_img();
                                        endif;
                                        ?>
                                    </div><!-- /ft-book-img-->

                                </div><!-- feature-book -->
                            </div><!-- col-md-6 -->
                        <?php
                        endwhile;
                        wp_reset_query();
                    endif;
                    ?>
                </div><!-- /feature slider wrapper -->
            </div><!-- feature-slider-wrapper-section -->
        </div><!-- /row -->
    </div><!-- /container -->

</div><!-- feature book wrapper -->