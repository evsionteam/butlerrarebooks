<!--Recent Books wrapper-->
<div class="recent-books-wrapper">
    <div class="container">
        <div class="row">

            <!--recent-books-title-->
            <div class="col-md-12">
                <div class="recent-books-title">
                    <h1><?php _e('Recently Added Books', 'butlerrarebooks'); ?></h1>
                    <div class="view-all-books">
                        <a href="<?php echo get_post_type_archive_link('product'); ?>"><?php _e('View All', 'butlerrarebooks'); ?></a>
                    </div>
                </div><!-- /recent-books-title -->
            </div><!-- col-md-12 -->

            <?php
            $args = array(
                'post_type' => 'product',
                'posts_per_page' => 4,
            );
            $recent_books = new WP_Query($args);
            if ($recent_books->have_posts()) :
                while ($recent_books->have_posts()) :$recent_books->the_post();
                    global $product;
                    ?>
                    <div class="col-xs-6 col-sm-4 col-lg-3 hvr-bob">
                        <div class="butler-books">
                            <div class="ft-book-img">
                                <?php
                                if(has_post_thumbnail()):
                                    the_post_thumbnail('shop_catalog');
                                else:
                                    echo wc_placeholder_img();
                                endif;
                                ?>
                            </div>
                            <div class="butler-book-title">
                                <a href="<?php the_permalink() ?>"><h2><?php echo $product->get_title(); ?></h2></a>
                            </div>
                            <?php $price = $product->get_price();if(!empty($price)):?>
                                <div class="butler-book-price">
                                    <h3>
                                        <?php
                                        echo get_woocommerce_currency_symbol();
                                        echo $price;
                                        ?>
                                    </h3>
                                </div>
                            <?php endif;?>

                            <div class="ft-book-info">
                                <?php
                                //book author
                                $book_author = get_post_meta(get_the_ID(), 'book-author', true);
                                if (!empty($book_author)):
                                    echo '<div class="book-author">';
                                    echo '<span>' . __('Author', 'butlerrarebooks') . '</span>:' . $book_author;
                                    echo '</div>';
                                endif;

                                //publication date
                                $book_published = get_post_meta(get_the_ID(), 'book-published', true);
                                if (!empty($book_published)):
                                    echo '<div class="book-published">';
                                    echo '<span>' . __('Publication Date', 'butlerrarebooks') . '</span>:' . $book_published;
                                    echo '</div>';
                                endif;
                                ?>
                            </div>

                            <div class="brb-more">
                                <a href="<?php the_permalink() ?>"><?php _e('More', 'butlerrarebooks') ?></a>
                            </div>
                        </div>
                    </div><!-- col-md-4 -->
                <?php
                endwhile;
                wp_reset_query();
            endif;
            ?>
        </div><!-- /row -->
    </div><!-- /container -->
</div><!-- /Recent Books wrapper -->