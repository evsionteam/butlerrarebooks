<!-- search-banner section -->
<div class="search-banner-section">
    <div class="col-md-3">
        <!--browse-categories-->
        <div class="browse-categories">
            <?php
            /* Browse By Categories */
            //fetch only parent categories
            $book_categories = get_terms('product_cat', array('hide_empty' => 0,'fields'=>'id=>name','parent' => 0));
            if (!empty($book_categories) && !is_wp_error($book_categories)) {
                echo '<div class="browse-by-categories">';
                echo '<h3>' . __('Browse By Categories', 'butlerrarebooks') . '</h3>';
                echo '<ul id="bro-cat">';
                foreach ($book_categories as $book_id => $book_name) {
                    $book_link = get_term_link($book_id);
                    if (is_wp_error($book_link)) {
                        continue;
                    }
                    echo '<li>';
                    echo '<a href="' . esc_url($book_link) . '">' . $book_name . '</a>';
                    //fetch child categories if any
                    $sub_cats = get_term_children( $book_id, 'product_cat' );
                    if(!empty($sub_cats)){
                        echo '<ul>';
                        foreach ($sub_cats as $sub_cat){
                            $sub_cat_link = get_term_link($sub_cat);
                            if (is_wp_error($sub_cat_link)) {
                                continue;
                            }
                            echo '<li class="book-sub-cats"><a href="' . esc_url($sub_cat_link) . '">' . get_cat_name($sub_cat) . '</a></li>';
                        }
                        echo '</ul>';
                    }
                    echo '</li>';
                }
                echo '</ul>';
                echo '</div>';
            }
            /**/
            ?>
        </div><!-- /browse-categories -->
    </div><!-- col-md-3 -->

    <div class="col-md-9">

        <!--search and banner section-->
        <div class="search-n-banner-section">
            <!--search-section-->
            <div class="search-section">
                <div class="search-by-categories">
                    <form role="search" method="get" class="search-form" action="<?php echo home_url('/'); ?>">
                        <?php
                        $book_drop_categories = get_terms('product_cat', array('hide_empty' => 0));
                        if (!empty($book_drop_categories) && !is_wp_error($book_drop_categories)):
                            ?>
                            <div class="select-category-search" id="open-select">
                                <select name="category">
                                    <option value="all"><?php _e('All Categories', 'butlerrarebooks') ?></option>
                                    <?php
                                    foreach ($book_drop_categories as $book_category) {
                                        echo '<option value="' . $book_category->slug . '">' . $book_category->name . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        <?php endif; ?>
                        <div class="brb-search-cat-form">
                            <input type="search" class="search-field" placeholder="<?php echo esc_attr_x('Enter Keywords Here..', 'butlerrarebooks') ?>" value="<?php echo get_search_query() ?>" name="s" />
                            <input type="submit" class="search-submit" value="<?php echo esc_attr_x('Search', 'butlerrarebooks') ?>" />
                        </div>
                    </form>
                </div><!-- /search-section -->
            </div><!-- /search-n-banner-section -->

            <!--Banner section-->
            <?php
            $args = array(
                'post_type' => 'product',
                'meta_query' => array(
                    array(
                        'key' => 'book-in-slider',
                        'value' => 'yes',
                        'compare' => true
                    )
                )
            );
            $books_in_slider = new WP_Query($args);
            if ($books_in_slider):
                ?>
                <div class="banner-section">
                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <?php
                            $i = 0;
                            while ($books_in_slider->have_posts()):$books_in_slider->the_post();
                                ?>
                                <li data-target="#carousel-example-generic" data-slide-to="<?php echo $i; ?>" <?php
                                if (0 == $i): echo 'class="active"';
                                endif;
                                ?>></li>
                                    <?php
                                    $i++;
                                endwhile;
                                ?>
                        </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                            <?php
                            $active = true;
                            while ($books_in_slider->have_posts()):$books_in_slider->the_post();
                                global $product;
                                ?>
                                <div class="item <?php
                                if ('true' == $active): echo 'active';
                                    $active = false;
                                endif;
                                ?>" style="background-image:url('<?php echo get_template_directory_uri() ?>/images/slider1.jpg'); background-size:cover;">
                                    <!--<img src="<?php echo get_template_directory_uri() ?>/images/slider1.jpg">-->
                                    <div class="carousel-caption-custom">
                                        <div class="row">
                                            <div class="col-xs-7 col-sm-8">
                                                <div class="production-description">
                                                    <div class="book-title">
                                                        <?php the_title(); ?>
                                                    </div>
                                                    <?php
                                                    $product_price = $product->get_price();
                                                    if (!empty($product_price)):
                                                        ?>
                                                        <div class="book-price">
                                                            <?php
                                                            echo get_woocommerce_currency_symbol();
                                                            echo $product_price;
                                                            ?>
                                                        </div>
                                                    <?php endif; ?>
                                                    <?php
                                                    $product_author = get_post_meta(get_the_ID(), 'book-author', true);
                                                    if (!empty($product_author)):
                                                        ?>
                                                        <div class="book-author">
                                                            <?php echo __('Author', 'butlerrarebooks') . ':' . $product_author; ?>
                                                        </div>
                                                    <?php endif; ?>
                                                    <div class="get-book">
                                                        <a href="<?php the_permalink(); ?>">
                                                            <?php _e('More Details', 'butlerrarebooks'); ?>
                                                        </a>
                                                    </div>
                                                </div><!-- product description -->
                                            </div><!-- col-md-7 -->

                                            <div class="col-xs-5 col-sm-4 col-sm-4">
                                                <div class="banner-img-sec">
                                                    <?php
                                                    if (has_post_thumbnail()):
                                                        the_post_thumbnail('brb-main-slider');
                                                    else:
                                                        echo wc_placeholder_img();
                                                    endif;
                                                    ?>
                                                </div><!-- banner-img-sec -->
                                            </div><!-- col-sm-4 -->

                                        </div><!-- row -->
                                    </div><!-- carousel-caption -->

                                </div>
                                <?php
                            endwhile;
                            wp_reset_postdata();
                            ?>
                        </div>

                        <!-- Controls -->
                        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                            <span class="glyphicon" aria-hidden="true"><img src="<?php echo get_template_directory_uri() ?>/images/left-arrow.png" alt="left arrow"/></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                            <span class="glyphicon" aria-hidden="true"><img src="<?php echo get_template_directory_uri() ?>/images/right-arrow.png" alt="left arrow"/></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div><!-- /carousel -->
                </div><!-- /banner section -->

            <?php endif; ?>
        </div><!-- col-md-9 -->
    </div><!-- /search-banner-section -->
</div><!-- /search-banner-section -->