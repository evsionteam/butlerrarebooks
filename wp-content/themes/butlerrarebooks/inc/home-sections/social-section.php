<?php global $brb_options;?>
<!--Newsletter and social links section-->
<div class="col-md-12">
    <div class="newsletter-n-social-links">
        <div class="brb-social-options">
            <div class="row">
                <div class="col-md-7 col-lg-8">
                    <div class="brb-newsletter clearfix">
                        <h3> <?php _e('Sign up for our catalogues','butlerrarebooks'); ?></h3><?php
                        echo do_shortcode('[contact-form-7 id="65" title="Subscribe Form"]');
                        ?>
                    </div>
                </div>
                <div class="col-md-5 col-lg-4">
                    <div class="brb-social-links">
                        <span> <?php _e('Connect With Social links', 'butlerrarebooks'); ?></span>
                        <ul>
                            <?php $fb_link = $brb_options['brb_facebook_link'];if(!empty($fb_link)):?>
                                <li><a href="<?php echo $fb_link;?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
                            <?php endif;?>
                            <?php $twitter_link = $brb_options['brb_twitter_link'];if(!empty($twitter_link)):?>
                                <li><a href="<?php echo $twitter_link;?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
                            <?php endif;?>
                            <?php $youtube_link = $brb_options['brb_youtube_link'];if(!empty($youtube_link)):?>
                                <li><a href="<?php echo $youtube_link;?>" target="_blank"><i class="fa fa-youtube"></i></a></li>
                            <?php endif;?>
                            <?php $linkedin_link = $brb_options['brb_linkedin_link'];if(!empty($linkedin_link)):?>
                                <li><a href="<?php echo $linkedin_link;?>" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                            <?php endif;?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- newsletter-n-social-links -->
</div><!-- col-md-12 -->
