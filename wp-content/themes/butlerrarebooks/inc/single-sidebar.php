<?php
/**
 * Created by PhpStorm.
 * User: Rubal
 * Date: 1/6/16
 * Time: 1:48 PM
 */
?>
<div class="brb-newsletter inner clearfix">
    <h3>
        <?php _e('Sign up for our catalogues','butlerrarebooks'); ?>
    </h3>
    <?php echo do_shortcode('[contact-form-7 id="65" title="Subscribe Form"]');?>
</div>
<!--browse-categories-->
<div class="browse-categories inner">
    <?php
    /* Browse By Categories */
    //fetch only parent categories
    $book_categories = get_terms('product_cat', array('hide_empty' => 0,'fields'=>'id=>name','parent' => 0));
    if (!empty($book_categories) && !is_wp_error($book_categories)) {
        echo '<div class="browse-by-categories">';
        echo '<h3>' . __('Browse By Categories', 'butlerrarebooks') . '</h3>';
        echo '<ul id="bro-cat">';
        foreach ($book_categories as $book_id => $book_name) {
            $book_link = get_term_link($book_id);
            if (is_wp_error($book_link)) {
                continue;
            }
            echo '<li>';
            echo '<a href="' . esc_url($book_link) . '">' . $book_name . '</a>';
            //fetch child categories if any
            $sub_cats = get_term_children( $book_id, 'product_cat' );
            if(!empty($sub_cats)){
                echo '<ul>';
                foreach ($sub_cats as $sub_cat){
                    $sub_cat_link = get_term_link($sub_cat);
                    if (is_wp_error($sub_cat_link)) {
                        continue;
                    }
                    echo '<li class="book-sub-cats"><a href="' . esc_url($sub_cat_link) . '">' . get_cat_name($sub_cat) . '</a></li>';
                }
                echo '</ul>';
            }
            echo '</li>';
        }
        echo '</ul>';
        echo '</div>';
    }
    /**/
    ?>
</div><!-- /browse-categories -->