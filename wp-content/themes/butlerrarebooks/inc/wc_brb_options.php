<?php
/**
 * Created by PhpStorm.
 * User: Rubal
 * Date: 12/30/15
 * Time: 9:14 AM
 */

//add meta boxes in product page
function brb_product_info() {
    add_meta_box( 'brb_product', __( 'Book Info ', 'butlerrarebooks' ), 'brb_product_info_callback', 'product' );
}
add_action( 'add_meta_boxes', 'brb_product_info' );

/**
 * Outputs the content of the meta box
 */
function brb_product_info_callback( $post ) {
    wp_nonce_field( basename( __FILE__ ), 'brb_nonce' );
    $brb_stored_meta = get_post_meta( $post->ID );
    ?>
    <table id="book-info-wrap">
        <tbody>
        <tr>
            <td class="left">
                <label for="book-author" class="brb-row-title">
                    <?php _e( 'Author:', 'butlerrarebooks' )?>
                </label>
            </td>
            <td>
                <input type="text" name="book-author" id="book-author" value="<?php if ( isset ( $brb_stored_meta['book-author'] ) ) echo $brb_stored_meta['book-author'][0]; ?>" size="30" />
            </td>
        </tr>
        <tr>
            <td class="left">
                <label for="book-published" class="brb-row-title">
                    <?php _e( 'Publication Date:', 'butlerrarebooks' )?>
                </label>
            </td>
            <td>
                <input type="text" name="book-published" id="book-published" value="<?php if ( isset ( $brb_stored_meta['book-published'] ) ) echo $brb_stored_meta['book-published'][0]; ?>" size="30" />
            </td>
        </tr>
        <tr>
            <td class="left">
                <label for="book-in-slider" class="brb-row-title">
                    <?php _e( 'Show in Slider?', 'butlerrarebooks' )?>
                </label>
            </td>
            <td>
                <input type="checkbox" name="book-in-slider" id="book-in-slider" value="yes" <?php if ( isset ( $brb_stored_meta['book-in-slider'] ) ) checked( $brb_stored_meta['book-in-slider'][0], 'yes' ); ?> />
                <?php _e( 'Yes', 'butlerrarebooks' )?>
            </td>
        </tr>
        </tbody>
    </table>
<?php
}

/**
 * Saves the custom meta input
 */
function brb_meta_save( $post_id ) {
    // Checks save status
    $is_autosave = wp_is_post_autosave( $post_id );
    $is_revision = wp_is_post_revision( $post_id );
    $is_valid_nonce = ( isset( $_POST[ 'brb_nonce' ] ) && wp_verify_nonce( $_POST[ 'brb_nonce' ], basename( __FILE__ ) ) ) ? 'true' : 'false';
    // Exits script depending on save status
    if ( $is_autosave || $is_revision || !$is_valid_nonce ) {
        return;
    }
    if( isset( $_POST[ 'book-published' ] ) ) {
        update_post_meta( $post_id, 'book-published', sanitize_text_field( $_POST[ 'book-published' ] ) );
    }

    if( isset( $_POST[ 'book-author' ] ) ) {
        update_post_meta( $post_id, 'book-author', sanitize_text_field( $_POST[ 'book-author' ] ) );
    }

    if( isset( $_POST[ 'book-in-slider' ] ) ) {
        update_post_meta( $post_id, 'book-in-slider', 'yes' );
    } else {
        update_post_meta( $post_id, 'book-in-slider', '' );
    }
}
add_action( 'save_post', 'brb_meta_save' );


//remove woocommerce cart button
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
remove_action( 'woocommerce_simple_add_to_cart', 'woocommerce_simple_add_to_cart', 30 );
remove_action( 'woocommerce_grouped_add_to_cart', 'woocommerce_grouped_add_to_cart', 30 );


//add new tab "Tags" in product description page
add_filter( 'woocommerce_product_tabs', 'brb_woo_add_tabs', 11 );
function brb_woo_add_tabs( $tabs ) {
    unset($tabs['reviews']);
    // Adds Product tags tab
    $tabs['products_tags_tab'] = array(
        'title'     => __( 'Product Tags', 'woocommerce' ),
        'priority'  => 15,
        'callback'  => 'woo_product_tag_tab_content'
    );

    return $tabs;
}
function woo_product_tag_tab_content() {
    global $post, $product;
    $tag_count = sizeof( get_the_terms( $post->ID, 'product_tag' ) );
    echo $product->get_tags( ', ', '<span class="tagged_as">' . _n( 'Tag:', 'Tags:', $tag_count, 'woocommerce' ) . ' ', '.</span>' );
}


//add product meta and form in product page
remove_action('woocommerce_single_product_summary','woocommerce_template_single_meta',40);
remove_action('woocommerce_single_product_summary','woocommerce_template_single_excerpt',20);
add_action('woocommerce_single_product_summary','woocommerce_template_single_excerpt',8);

add_action('woocommerce_single_product_summary','brb_product_meta_info',9);
function brb_product_meta_info(){

    echo '<hr>';
    //book author
    $book_author = get_post_meta(get_the_ID(), 'book-author', true);
    if (!empty($book_author)):
        echo '<div class="book-author">';
        echo '<span>' . __('Author', 'butlerrarebooks') . '</span>:' . $book_author;
        echo '</div>';
    endif;

    //publication date
    $book_published = get_post_meta(get_the_ID(), 'book-published', true);
    if (!empty($book_published)):
        echo '<div class="book-published">';
        echo '<span>' . __('Publication Date', 'butlerrarebooks') . '</span>:' . $book_published;
        echo '</div>';
    endif;
}

//add contact form in single product page
add_action('woocommerce_single_product_summary','brb_product_contact_form',11);
function brb_product_contact_form(){
    ?>
    <!--email to friend-->
    <div class="brb_email_to_friend">
        <a class="btn btn-primary btn-lg" data-toggle="modal" data-target="#emailModal">
            <?php _e('Email to a Friend','butlerrarebooks'); ?>
        </a>
    </div>
    <div class="modal fade" id="emailModal" tabindex="-1" role="dialog" aria-labelledby="emailModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="emailModalLabel"><?php _e('Email this Product to Friend','butlerrarebooks');?></h4>
                </div>
                <div class="modal-body">
                    <?php echo do_shortcode('[contact-form-7 id="77" title="Email To friend Form"]');?>
                </div>
            </div>
        </div>
    </div>
    <!--email close-->

    <!--enquire-->
    <button type="button" class="btn btn-primary btn-lg btn-enquiry" data-toggle="modal" data-target="#myform">
        <?php _e('Enquire about this product','butlerrarebooks');?>
    </button>
    <div class="modal fade" id="myform" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title" id="myModalLabel"><?php _e('Contact Us','butlerrarebooks');?></h3>
                </div>
                <div class="modal-body">
                    <h4><?php _e('Contact Information','butlerrarebooks');?></h4>
                    <?php echo do_shortcode('[contact-form-7 id="73" title="Product Contact Form"]');?>
                </div>
            </div>
        </div>
    </div>
    <!--enquire close-->

<?php
}

//change priority of breadcrumb
remove_action( 'woocommerce_before_main_content','woocommerce_breadcrumb',20 );
add_action( 'woocommerce_before_main_content','woocommerce_breadcrumb',8 );

add_action( 'woocommerce_before_main_content','woocommerce_breadcrumb_brb_wrapper_start',7 );
add_action( 'woocommerce_before_main_content','woocommerce_breadcrumb_brb_wrapper_end',9 );
function woocommerce_breadcrumb_brb_wrapper_start(){
    echo '<div class="brb_woo_breadcrumb_wrapper"><div class="container">';
}
function woocommerce_breadcrumb_brb_wrapper_end(){
    echo '</div></div>';
}


//add wrapper for sorting options in product archive
add_action( 'woocommerce_before_shop_loop','woocommerce_sorting_brb_wrapper_start',25 );
add_action( 'woocommerce_before_shop_loop','woocommerce_sorting_brb_wrapper_end',35 );
function woocommerce_sorting_brb_wrapper_start(){
    echo '<div class="col-md-4">';
}
function woocommerce_sorting_brb_wrapper_end(){
    echo '</div>';
}


//remove woocommerce orderby options
function brb_woocommerce_catalog_orderby( $orderby ) {
    unset($orderby["rating"]);
    return $orderby;
}
add_filter( "woocommerce_catalog_orderby", "brb_woocommerce_catalog_orderby", 20 );

//add author in product display
add_action( 'woocommerce_after_shop_loop_item_title','brb_book_author',6 );
function brb_book_author(){
    //book author
    $book_author = get_post_meta(get_the_ID(), 'book-author', true);
    if (!empty($book_author)):
        echo '<div class="book-author">';
        echo '<span>' . __('By', 'butlerrarebooks') . '</span> ' . $book_author;
        echo '</div>';
    endif;
}