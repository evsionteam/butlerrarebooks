
//OPEN SELECT
(function ($) {
    "use strict";
    $.fn.openSelect = function ()
    {
        return this.each(function (idx, domEl) {
            if (document.createEvent) {
                var event = document.createEvent("MouseEvents");
                event.initMouseEvent("mousedown", true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
                domEl.dispatchEvent(event);
            } else if (element.fireEvent) {
                domEl.fireEvent("onmousedown");
            }
        });
    }
}(jQuery));



//Feature slider
(function ($) {
    $(document).ready(function ($) {

        // user slider
        $(".feature-slider-wrapper").carouFredSel({
            auto: false,
            infinite: true,
            responsive: true,
            width: '100%',
            scroll: 1,
            items: {
                visible: {
                    min: 1,
                    max: 2
                }
            },
            prev: {
                button: "#pre",
                key: "left"
            },
            next: {
                button: "#next",
                key: "right"
            }
        });

        // nice scroll
        $(".browse-categories ul").niceScroll({cursorcolor:"#EB5C01", cursorborder:"none", cursoropacitymin:"1"});
        
        //for select option click
        $("#open-select").on("click", function (e) {
            e.stopPropagation();
            $(this).toggleClass("active");
            if ($(this).hasClass("active")) {
                $("#open-select select").openSelect();
            }
            ;
        });
        $("#open-select select").on("click", function (e) {
            e.stopPropagation();
            $(this).parent("#open-select").toggleClass("active");
        });

        var priceFilter = $(".widget_price_filter").length;
        if ( priceFilter == 0)
        {
            $(".brb-price-filter").hide();
        }
    });

})(jQuery);



// For browser by categories and main menu
(function ($) {
    'use strict';

    //constructor method 
    var menuToggler = function (arg) {
        this.btn = arg.btn;
        this.container = arg.container;
        this.init();

        var that = this;
        $(window).resize(function () {
            that.init();
        });
    };

    //method
    menuToggler.prototype.init = function () {
        var windowWidth = $(window).width();
        if (windowWidth <= 974) {
            var that = this;
            if (!$(this.btn).hasClass("active")) {
                $(this.btn).addClass("active");
                $(this.btn).on("click", function () {
                    $(that.container).slideToggle();
                });
                $(this.container).hide();
            }

        } else
        {
            if ($(this.btn).hasClass("active")) {
                $(this.btn).removeClass("active").unbind("click");
                $(this.container).show();
            }
        }
    }

    //creating object
    var browseCategories = new menuToggler({"btn": ".browse-by-categories h3", "container": ".browse-by-categories h3 + ul"});
    var mainMenu = new menuToggler({"btn": ".menu-toggle", "container": ".menu-toggle + .menu-primary-menu-container"});

}(jQuery));


/* ========================================================================
 * MORE MENU v1
 * Inspired by Bootstrap
 * ======================================================================== */
(function ($) {
    'use strict';

    //    constructor method here moremenu is class
    var moreMenu = function (arg) {
        this.selector = arg.selectorName;
        this.init();
    }

    //methods
    moreMenu.prototype.init = function () {
        $(this.selector + " > a").on("click", function (e) {
            e.preventDefault();
            $("+ ul", this).slideToggle();
        });
    };

    //creating objects of class
    var obj = new moreMenu({"selectorName": ".more-nav"});

}(jQuery));

