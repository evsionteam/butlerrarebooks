<?php
/**
 * Created by PhpStorm.
 * User: Rubal
 * Date: 12/29/15
 * Time: 1:56 PM
 */
/* Template Name: Contact us */
get_header();
?>

<div class="brb_woo_breadcrumb_wrapper">
    <div class="container">
        <?php woocommerce_breadcrumb(); ?>
    </div>
</div>

<!--contact us-->
<div id="primary" class="content-area container contact-us">
    <main id="main" class="site-main" role="main">
        <div class="row">
            
            <!--contact form section-->
            <div class="col-md-6">
                <div class="contact-form-section common-form">
                    <?php echo do_shortcode('[contact-form-7 id="70" title="Contact Form"]');?>
                </div><!-- /contact-form-section -->
            </div><!-- col-md-6 -->
            
            <!--contact information section-->
            <div class="col-md-6">
                <div class="contact-info">
                    
                </div><!-- /contact-detail -->
            </div><!-- col-md-6 -->
            
        </div>
    </main><!-- /main -->
</div><!-- contact us -->

<!--including footer section-->
<?php
get_footer();
