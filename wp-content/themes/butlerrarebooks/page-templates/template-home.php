<?php
/**
 * Created by PhpStorm.
 * User: Rubal
 * Date: 12/29/15
 * Time: 1:56 PM
 */
/* Template Name: Home Page Template */
get_header();
?>
<!-- MAIN SECTION-->
<main>
    <!--Search Banner section and introduction section wrapper-->
    <div class="search-banner-section-n-intro-section">
        <div class="container">

            <div class="row">

                <!-- search-banner section -->
                <?php get_template_part('inc/home-sections/search','section');?>

                <!--Newsletter and social links section-->
                <?php get_template_part('inc/home-sections/social','section');?>

                <!--ABOUT US SECTION-->
                <?php get_template_part('inc/home-sections/about','section');?>

            </div><!-- /row -->
        </div><!-- /container -->
    </div><!-- /search-banner-section-n-intro-section -->

    <!--Featured Books wrapper-->
    <?php get_template_part('inc/home-sections/featured','books');?>

    <!--Recent Books-->
    <?php get_template_part('inc/home-sections/recent','books');?>

</main><!-- /end main -->

<!--including footer section-->
<?php
get_footer();
