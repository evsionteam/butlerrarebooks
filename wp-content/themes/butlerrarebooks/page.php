<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package butlerrarebooks
 */
get_header();
?>

<div class="brb_woo_breadcrumb_wrapper">
    <div class="container">
        <?php woocommerce_breadcrumb(); ?>
    </div>
</div>

<div id="primary" class="content-area container">
    <main id="main" class="site-main" role="main">
        <div class="row">
            <?php while (have_posts()) : the_post(); ?>
                <div class="col-sm-5 col-md-4">
                    <div class="img-wrapper">
                        <?php the_post_thumbnail('medium'); ?>
                    </div>
                </div>
                <div class="col-sm-7 col-md-8">
                    <div class="content-description">
                        <?php
                        get_template_part('template-parts/content', 'page');
                    endwhile; // End of the loop.
                    ?>
                </div>
            </div>
        </div>
    </main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
