<?php
/**
 * The template for displaying search results pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package butlerrarebooks
 */
get_header();
?>

<section id="primary" class="content-area container common-inner-page search-page">
    <main id="main" class="site-main" role="main">

        <?php if (have_posts()) : ?>

            <header class="page-header">
                <h3 class="page-title"><?php printf(esc_html__('Search Results for: %s', 'butlerrarebooks'), '<span>' . get_search_query() . '</span>'); ?></h3>
            </header><!-- .page-header -->

            <div class="row">
                <?php
                /* Start the Loop */
                while (have_posts()) : the_post();

                    /**
                     * Run the loop for the search to output the results.
                     * If you want to overload this in a child theme then include a file
                     * called content-search.php and that will be used instead.
                     */
                    get_template_part('template-parts/content', 'search');

                endwhile;
                ?>
                <div class="col-md-12">
                    <?php
                    brb_pagination();
                    ?>
                </div>
                <?php
            else :

                get_template_part('template-parts/content', 'none');

            endif;
            ?>
        </div>
    </main><!-- #main -->
</section><!-- #primary -->

<?php
get_footer();
