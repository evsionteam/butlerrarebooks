<?php
/**
 * Template part for displaying results in search pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package butlerrarebooks
 */
?>
<div class="col-md-6">
    <article class="sigle-search" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <div class="row">
            <div class="col-md-4">
                <div class="brb-ft-img">
                    <?php the_post_thumbnail('thumbnail'); ?>
                </div>
            </div><!-- col-md-5 -->
            
            <div class="col-md-8">
                <header class="entry-header">
                    <?php the_title(sprintf('<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url(get_permalink())), '</a></h2>'); ?>

                    <?php if ('post' === get_post_type()) : ?>
                        <div class="entry-meta">
                            <?php butlerrarebooks_posted_on(); ?>
                        </div><!-- .entry-meta -->
                    <?php endif; ?>
                </header><!-- .entry-header -->

                <div class="entry-summary">
                    <?php the_excerpt(); ?>
                </div><!-- .entry-summary -->
            </div><!-- col-md-7 -->
        </div><!-- /row -->

        <!--<footer class="entry-footer">
        <?php /* butlerrarebooks_entry_footer(); */ ?>
        </footer><!-- .entry-footer -->
    </article><!-- #post-## -->
</div>